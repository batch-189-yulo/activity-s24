const getCube = 2 ** 3
message = `The cube of 2 is ${getCube}`
console.log(message)

/**/

const address = [53, "Luna.St", "Katarungan Village", "Antipolo City"];
const [houseNum, houseStreet, houseVillage, houseCity] = address
console.log(`I live at ${houseNum} ${houseStreet} ${houseVillage} ${houseCity}`)

/**/

let lolong = {
	habitat: "saltwater",
	weight: "300kg",
	length: "50ft"
}
let {habitat, weight, length} = lolong
console.log(`Lolong is a ${habitat} crocodile. He weighed ${weight} with a measurement of ${length}.`)

/**/

let arrayNum = [ 1, 2, 3, 4, 5]
arrayNum.forEach(function(arrayEa){
	console.log(`${arrayEa}`)
})

/**/

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed

	}
}

const firstDog = new Dog ("Ziggy", 1, "Jack Russel Terrier")
console.log(firstDog)
const secondDog = new Dog ("Wagyu", 1, "Shih Tzu")
console.log(secondDog)
const thirdDog = new Dog ("Violet", 2, "Pitbull Terrier")
console.log(thirdDog)
